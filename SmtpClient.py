#!/usr/local/bin/python
import smtplib
import sys
import time
import ConfigParser
import base64
import os
import mimetypes
from optparse import OptionParser
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import formatdate
from email import Encoders
from smtplib import SMTPResponseException

class SMTPClient:
    def __init__(self):
        self.config = dict();
        self.config['from']        = ''
        self.config['to']          = ''
        self.config['subject']     = 'Test mail from SMTPClient'
        self.config['attach']      = 'none'
        self.config['body']        = 'This is a test main ^MLine2'
        self.config['mailfile']    = ''
        self.config['server']      = ''
        self.config['port']        = 25
        self.config['login']       = ''
        self.config['password']    = ''
        self.config['authmode']    = 'login'
        self.config['sleep']       = 0
        self.config['debug']       = False
        self.config['ssl']         = False
        self.config['mailcount']   = 1
        self.config['headers']     = ''
        self.config['numprocess']  = 1
        self.HEADER                = ['Sent-By','SMTPClient']

    def loadConfigFile(self,filename,section='DEFAULT'):
        cfg = ConfigParser.ConfigParser()
        cfg.read(cmdline.configFile);

        for key in self.config.keys():
            val =  cfg.get(section,key);
            if (val != None) :
                print section
                print val
                self.config[key] = val

    def isTrue(self,value):
        return value.lower() not in ['false','0','none',False,''];


    def setConfig(self,name,value):
        name= name.lower();
        if (self.config.has_key(name)):
            if(0==value.find('file:')):
                f=open(value[5,:],"r")
                value = f.read();
                f.close();
            self.config[name]=value

    #*******************************
    # Compose the message
    #*******************************
    def composeMessage(self,mailFrom,mailTo,mailSubject,mailBody,headers,mailAttachments=[]):
        msg = MIMEMultipart() ;
        msg['From'] = mailFrom ;
        msg['To'] = mailTo ;
        msg['Date'] = formatdate(localtime=True) ;
        msg['Subject'] = mailSubject ;

        for h in headers:
            msg.add_header(h[0],h[1]);

        if (len(mailBody) > 0 ):
            msg.attach( MIMEText(mailBody) )
            
        for f in mailAttachments:
            mime_type = mimetypes.guess_type(f)
            m=mime_type[0].split('/')
            part = MIMEBase(m[0],m[1]);
            part.set_payload( open(f,"rb").read() )
            Encoders.encode_base64(part);
            if (m[0] != 'text' and m[0] != 'image') :
                part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(f))
            msg.attach(part);

        return msg.as_string();

        
    def sendMail(self) :
        #Compose the message
        message=''
        mail_from=self.config['from'];
        if (mail_from== "" ) :
            mail_from=self.config['login']

        if (self.config['mailfile'] != 'none'):
            f=open(self.config['mailfile'],"r")
            message = f.read();
            f.close();
        else:
            headers=[]
            headers.append(self.HEADER)
            hlist=self.config['headers'].replace("^M","^A").replace("\n","^A")
            for h in hlist.split(""):
                if (h.find(':')>0):
                    headers.append(h.split(':'))
            attachments=[]
            if (self.config['attach'] != 'none') :
                attachments = self.config['attach'].split(',');        
            message=self.composeMessage(mail_from, self.config['to'],self.config['subject'], self.config['body'],headers,attachments);

        
        print "Start : " ,time.asctime();
        try:
            for count in range(0,int( self.config['mailcount'])):
                print "Sending mail : ", count ;
                smtp = smtplib.SMTP();
                smtp.set_debuglevel(self.isTrue(self.config['debug']));        
                smtp.connect(self.config['server'],self.config['port'])
                smtp.ehlo();
        
                authmode=self.config['authmode'].lower();
                if ('login' == authmode or 'plain' == authmode) :
                    smtp.login(self.config['login'],self.config['password'])
            
                #sys.exit(1);
                for fromAddress in mail_from.split(','):        
                    if (int(self.config['sleep'])>0) : 
                        time.sleep(int(self.config['sleep']));
                smtp.sendmail(fromAddress, self.config['to'], message);
        finally:
            #pass
            smtp.quit()
    
        print "End   : " ,time.asctime();

#*********************************************************************************************
# Main Module
#*********************************************************************************************
if __name__ == "__main__":
    cmdlineParser = OptionParser()
    cmdlineParser.add_option("-f","--file" ,dest="configFile" , help="load config file" , default="smtp-config.ini");
    cmdlineParser.add_option("-r","--run-config" ,dest="configName" , help="Run Config data" , default="DEFAULT");
    cmdlineParser.add_option("-l","--list" ,action= "store_true", dest="listConfig" , help="List various configurations" , default=False);
    
    (cmdline,args) = cmdlineParser.parse_args()

    if cmdline.listConfig :
        cfg = ConfigParser.ConfigParser()
        cfg.read(cmdline.configFile);
        sections = cfg.sections()
        sections.sort();
        maxlen=max(map(len,sections)) + 15
        numCols=3
        maxrows=1+len(sections)/numCols
        print " The list of configurations avaliable : "
        for i in range(0,maxrows):
            row=''
            for k in range(0,numCols):
                j = k*maxrows+i
                if (j<len(sections)) :
                    r= "  ("
                    if j<10 :
                        r+=' '
                    r +=  str(j) +") " + sections[j]
                    row+= r.ljust(maxlen)
            print row
    else :    
        try :
            smtp = SMTPClient()
            smtp.loadConfigFile(cmdline.configFile,cmdline.configName);
            print "Num Processes : " , smtp.config['numprocess']
            print "user name :",smtp.config['login']
            if smtp.config['numprocess'] > 1 :
                for i in range(0,(int)(smtp.config['numprocess'])):
                    pid = os.fork();
		    if 0 == pid :
		       smtp.sendMail() 
		       break;
	    else:
            	smtp.sendMail()
        except:
            print "Exception Occured:", sys.exc_info()[1]

