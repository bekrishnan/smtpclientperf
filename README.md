This is a basic smtp client to perf test any smtp server or use it send multiple emails from the command line or programmatically.

smtp-config.ini contains a list of config settings - servers, logins, number of emails, processess etc, mostly self explanatory. 

Please mail me in case of issues or feature requests.

```
SmtpClient.py --list shows all the configs available
SmtpClient.py --run-config <config name from above list> runs that specific config or runs default
SmtpClient.py --file <filename> - for the use of anyother config file other than the defauly smtp-config.ini

```